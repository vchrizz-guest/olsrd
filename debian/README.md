# OLSRd - Optimized Link State Routing daemon
This repository is used to build Debian OLSRd packages with Jenkins.

### Table of Contents
* [Requirements](#requirements)
* [Setup Jenkins with jenkins-debian-glue](#setup-jenkins-with-jenkins-debian-glue)
* [Setup Debian repository](#setup-debian-repository)
* [Jenkins job **olsrd-source** (Freestyle project)](#jenkins-job-olsrd-source-freestyle-project)
* [Jenkins job **olsrd-binaries** (Multi-configuration project)](#jenkins-job-olsrd-binaries-multi-configuration-project)
* [Jenkins job **olsrd-piuparts** (Freestyle project)](#jenkins-job-olsrd-piuparts-freestyle-project)
* [Finally: run Jenkins job `olsrd-source`](#finally-run-jenkins-job-olsrd-source)
* [References](#references)

## Requirements

* Debian OS (must be physical or virtual machine - does not work properly within container like LXC or docker)
  * recommended Hardware (for 4 parallel build jobs): 4 vCPU + 12GB RAM + min. 50GB HDD
  * tested to work successfully on Debian 10/buster and 11/bullseye
* Jenkins
* jenkins-debian-glue

## Setup Jenkins with jenkins-debian-glue

```
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update
sudo apt install default-jdk jenkins jenkins-debian-glue pbuilder qemu-user-static ubuntu-keyring ubuntu-archive-keyring
sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

NOTE: to build for Ubuntu the packages **ubuntu-keyring ubuntu-archive-keyring** have to be installed from buster, as they are not available in bullseye (yet).

Access http://jenkins-host:8080/ and enter the initialAdminPassword to configure a login.

**IMPORTANT: From "Manage Jenkins" -> "Manage Plugins" -> "Available" install: "Copy Artifact Plugin"**

#### edit `/etc/jenkins/debian_glue` to define pbuilder configuration and path to debian repository

    ARCHITECTURES="amd64 i386 armhf armel mips mipsel source"
    PBUILDER_CONFIG=/etc/jenkins/pbuilderrc
    DEFAULT_REPOSITORY=/srv/debian
    REMOVE_FROM_RELEASE=true

#### create `/etc/jenkins/pbuilderrc` to write pbuilder configuration

    DEBIAN_SUITES=("stretch" "buster" "bullseye")
    DEBIAN_MIRROR="deb.debian.org"
    UBUNTU_SUITES=("xenial" "bionic" "focal")
    UBUNTU_MIRROR="mirrors.kernel.org"
    
    if $(echo ${DEBIAN_SUITES[@]} | grep -q $DIST); then
        # Debian configuration
        MIRRORSITE="http://$DEBIAN_MIRROR/debian/"
        COMPONENTS="main contrib non-free"
        PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-apt"
    elif $(echo ${UBUNTU_SUITES[@]} | grep -q $DIST); then
        # Ubuntu configuration
        if [ "$ARCH" == "arm64" ] || [ "$ARCH" == "armhf" ]; then
            #MIRRORSITE="http://ports.ubuntu.com/"
            MIRRORSITE="http://ftp.tu-chemnitz.de/pub/linux/ubuntu-ports/"
        else
            MIRRORSITE="http://$UBUNTU_MIRROR/ubuntu/"
        fi
        COMPONENTS="main restricted universe multiverse"
        PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-classic"
    else
        echo "Unknown distribution: $DIST"
        exit 1
    fi
    
    BASEPATH="/var/cache/pbuilder/base-${DIST}-${ARCH}.cow/"
    DISTRIBUTION=$DIST
    distribution=$DIST

#### edit `/usr/bin/generate-git-snapshot` to remove ${BUILD_VERSION} from pkg-names for reproducible builds

    sed 's/VERSION_STRING="${INCREASED_VERSION.*/VERSION_STRING="${INCREASED_VERSION}"/' /usr/bin/generate-git-snapshot

## Setup Debian repository
```
# generate keypair without password
sudo -i -u jenkins gpg2 --batch --passphrase '' --quick-gen-key "Custom Debian repository" default default never
# put fingerprint of generated subkey into jenkins-debian-glue configuration
key=$(sudo -i -u jenkins gpg2 --list-secret-key --with-subkey-fingerprint | awk '/ssb/{getline; print}' | xargs)
sed -i "s/.*KEY_ID=.*/KEY_ID=$key/" /etc/jenkins/debian_glue
# create repository
sudo mkdir -p /srv/debian/conf/
sudo touch /srv/debian/conf/distributions
sudo chown -R jenkins. /srv/debian
#sudo generate-reprepro-codename buster
# setup webserver
sudo apt install nginx
# change directory of debian repository to /srv
sudo sed -i '0,/root.*/s//root \/srv;/' /etc/nginx/sites-enabled/default
# enable directory listings
sudo sed -i '0,/location \/ {/s//location \/ {\n\t\tautoindex on;\n\t\tautoindex_localtime on;/' /etc/nginx/sites-enabled/default
# disallow access to conf/ and db/ from web
sudo sed -i '0,/location \/ {/s//location ~ conf\/$ { internal; }\n\tlocation ~ db\/$ { internal; }\n\tlocation \/ {/' /etc/nginx/sites-enabled/default
sudo systemctl reload nginx
# export key
sudo -i -u jenkins gpg2 --armor --output /srv/debian/repository.gpg.key --export-options export-minimal --export $key
```

## Jenkins job **olsrd-source** (Freestyle project)

* **Discard Old Builds**
  * Max # of builds to keep
    `10`
* **Source Code Management**
  * Git / Repository URL
    https://salsa.debian.org/vchrizz-guest/olsrd.git
  * Branches to build / Branch Specifier
    `*/master`
  * Additional Behaviours / Check out to a sub-directory
    `source`
* **Build Environment**
  * [x] Delete workspace before build starts
* **Build**
  * Execute shell
```
export GPG_TTY=$(tty)
rm -f ./* || true
/usr/bin/generate-git-snapshot
mkdir -p report
/usr/bin/lintian-junit-report *.dsc > report/lintian.xml
```
* **Post-build Actions**
  * Archive the artifacts
    `*.gz,*.bz2,*.xz,*.deb,*.dsc,*.git,*.changes,*.buildinfo,lintian.txt`

## Jenkins job **olsrd-binaries** (Multi-configuration project)

* **Discard Old Builds**
  * Max # of builds to keep
    `10`
* **Build Triggers**
  * Build after other projects are built
    `olsrd-source`
* **Configuration Matrix (Add axis)**
  * User-defined Axis (Name)
    `architecture`
  * User-defined Axis (Values)
    `amd64 i386 armel armhf mips mipsel`
  * User-defined Axis (Name)
    `distribution`
  * User-defined Axis (Values)
    `stretch buster bullseye xenial bionic focal`
  * Combination Filter
    `!(["bullseye","xenial","bionic","focal"].contains(distribution) && architecture=="mips") && !(["xenial","bionic","focal"].contains(distribution) && architecture=="mipsel") && !(["xenial","bionic","focal"].contains(distribution) && architecture=="armel")`
* **Build Environment**
  * [x] Delete workspace before build starts
* **Build**
  * Copy artifacts from another project (Project name)
    `olsrd-source`
  * Copy artifacts from another project (Artifacts to copy)
    `*`
  * Execute shell
```
export release=${distribution}
/usr/bin/build-and-provide-package
echo "Listing packages inside the $distribution repository:"
REPOSITORY="/srv/debian/release/$distribution" /usr/bin/repository_checker --list-repos $distribution
mkdir -p report
/usr/bin/lintian-junit-report *.dsc > report/lintian.xml
```
* **Post-build Actions**
  * Archive the artifacts
    `*.gz,*.bz2,*.xz,*.deb,*.dsc,*.changes,*.buildinfo`

## Jenkins job **olsrd-piuparts** (Freestyle project)
* **Discard Old Builds**
  * Max # of builds to keep
    `10`
* **Build Triggers**
  * Build after other projects are built
    `olsrd-binaries`
* **Build Environment**
  * [x] Delete workspace before build starts
* **Build**
  * Copy artifacts from another project (Project name)
    `olsrd-binaries/architecture=amd64,distribution=bullseye`
  * Copy artifacts from another project (Artifacts to copy)
    `*.deb`
  * Execute shell
```
sudo piuparts_wrapper *.deb
piuparts_tap piuparts.txt > piuparts.tap
```
* **Post-build Actions**
  * Archive the artifacts
    `**/piuparts.*`

## Finally: run Jenkins job `olsrd-source`

When all jobs successfully finished working (after about 1-2 hours) access following URLs to get packages:
- Debian:
  - http://jenkins-host/debian/release/stretch/pool/main/o/olsrd/
  - http://jenkins-host/debian/release/buster/pool/main/o/olsrd/
  - http://jenkins-host/debian/release/bullseye/pool/main/o/olsrd/
- Ubuntu:
  - http://jenkins-host/debian/release/xenial/pool/main/o/olsrd/
  - http://jenkins-host/debian/release/bionic/pool/main/o/olsrd/
  - http://jenkins-host/debian/release/focal/pool/main/o/olsrd/

Optional add repository to apt:
* OLSRd for Debian 9 (stretch):
```
sudo apt install gnupg2
echo 'deb http://jenkins-host/debian/release/stretch/ stretch main' | sudo tee /etc/apt/sources.list.d/olsrd.list
wget -qO - http://jenkins-host/debian/repository.gpg.key | sudo apt-key add -
sudo apt update
```
* OLSRd for Debian 10 (buster):
```
sudo apt install gnupg2
echo 'deb http://jenkins-host/debian/release/buster/ buster main' | sudo tee /etc/apt/sources.list.d/olsrd.list
wget -qO - http://jenkins-host/debian/repository.gpg.key | sudo apt-key add -
sudo apt update
```
* OLSRd for Debian 11 (bullseye):
```
sudo apt install gnupg2
echo 'deb http://jenkins-host/debian/release/bullseye/ bullseye main' | sudo tee /etc/apt/sources.list.d/olsrd.list
wget -qO - http://jenkins-host/debian/repository.gpg.key | sudo apt-key add -
sudo apt update
```

## References

- [jenkins-debian-glue](https://jenkins-debian-glue.org/faq/#single_repository "Can I collect multiple Debian packages in one single repository?")
- [jenkins-debian-glue - Setup Jenkins Jobs](https://jenkins-debian-glue.org/getting_started/manual/)
- [jdg-reprepro.yaml - sets up Jenkins jobs](https://github.com/mika/jenkins-debian-glue/blob/master/jjb/jdg-reprepro.yaml)
- [Building ARM Debian packages with pbuilder](https://jod.al/2015/03/08/building-arm-debs-with-pbuilder/ "help myself reproduce this pbuilder setup in the future")
- [cowbuilder - Building your package for many distributions at once](https://wiki.debian.org/cowbuilder#Building_your_package_for_many_distributions_at_once)
- [PbuilderTricks - How to build for different distributions](https://wiki.debian.org/PbuilderTricks#How_to_build_for_different_distributions "place a snippet in ~/.pbuilderrc to automate this")
- [ReproducibleBuildsHowto](https://wiki.debian.org/ReproducibleBuilds/Howto#Files_in_data.tar_contain_timestamps "Files contain timestamps" )
- [OpenPGP-Key-Management](https://www.gnupg.org/documentation/manuals/gnupg/OpenPGP-Key-Management.html "How to manage your keys")
- [SetupWithReprepro](https://wiki.debian.org/DebianRepository/SetupWithReprepro#Configuring_reprepro)
- [pbuilderrc manpage](https://manpages.debian.org/testing/pbuilder/pbuilderrc.5.en.html "Usage of PBUILDERSATISFYDEPENDSCMD")
- [no-versioned-debhelper-prerequisite](https://lintian.debian.org/tags/no-versioned-debhelper-prerequisite "The recommended practice is to always declare an explicit versioned dependency on debhelper equal to or greater than the compatibility level used by the package")
- [Common Debian pbuilder errors](http://flummox-engineering.blogspot.com/2012/12/common-debian-pbuilder-errors.html "Package Dependency Errors")
- [How can I generate a Git patch for a specific commit?](https://stackoverflow.com/questions/6658313/how-can-i-generate-a-git-patch-for-a-specific-commit)
- [How to apply a patch generated with git format-patch?](https://stackoverflow.com/questions/2249852/how-to-apply-a-patch-generated-with-git-format-patch)
- [Updating dpkg-buildflags to enable reproducible=+fixfilepath by default](https://lists.debian.org/debian-devel/2020/10/msg00222.html "disable this feature because it breaks building. add DEB_BUILD_MAINT_OPTIONS=reproducible=-fixfilepath to debian/rules")
- If version of olsrd is increased, a new tag in source git-repo with version in tagname has to be created. example: `upstream/0.9.9`
- eventually manually create cowbuilder base
  - `cowbuilder --create --basepath /var/cache/pbuilder/base-stretch-mipsel.cow --distribution stretch --debootstrapopts --arch --debootstrapopts mipsel`
  - and define to use it in pbuilderrc config with `BASEPATH="/var/cache/pbuilder/base-stretch-mipsel.cow/"`
- if: E: pbuilder-satisfydepends failed.
  - then in pbuilderrc use: `PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-apt"` (or -classic) instead default `pbuilder-satisfydepends-aptitude`
- if: Dependency is not satisfiable: debhelper (>= 9)
  - then in pbuilderrc use: `PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-apt"` (or -classic) instead `pbuilder-satisfydepends-gdebi`
- pbuilder build environments are in `/var/cache/pbuilder`
  - root@jenkins:/var/cache/pbuilder# chroot base-buster-mipsel.cow
